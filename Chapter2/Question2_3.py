'''
Created on Jan 19, 2015

@author: Yin Song
'''
class Solution(object):
    
    def delNode(self, linkedlistnode):    
        if (linkedlistnode == None or linkedlistnode.next == None):
            return False
        
        linkedlistnode2 = linkedlistnode.next
        linkedlistnode.data = linkedlistnode2.data
        linkedlistnode.next = linkedlistnode2.next
        
        return True