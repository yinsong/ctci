'''
Created on Jun 13, 2014

@author: Yin Song
'''
from .LinkedList import LinkedListNode

class Solution(object):
    '''
    Write code to remove duplicates from an unsorted linked
    list. FOLLOW UP How would you solve this problem 
    if a temporary buffer is not allowed?
    '''
    @classmethod
    def removeDup(self, linkedlist):
        # Check input
        if not isinstance(linkedlist, LinkedListNode):
            raise TypeError()
        
        # generate a dict for store data
        data_dict = dict()
        
        # for each node in the linkedlist
        # record previous node
        pre = None
        cur = linkedlist
        while(cur != None):
            if data_dict.has_key(cur.data):
                pre.next = cur.next
            else:
                data_dict[cur.data] = 1
            pre = cur
            cur = cur.next
            
        return linkedlist
            


        