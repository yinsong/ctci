'''
Created on Jan 19, 2015

@author: Yin Song
'''
import unittest
from ..LinkedList import LinkedListNode as Node
from ..Question2_3 import Solution as TestClass

class Test(unittest.TestCase):

    def createLinkedlist(self, data_list):
        """
           create linked list.
        """
        head = Node(data_list[0])
        cur = head
        for data in data_list[1:]:
            cur.setNext(Node(data))
            cur = cur.next
        return head
    
    def printLinkedlist(self, head):
        """
           print linked list.
        """
        data_list = []
        cur = head
        while(cur != None):
            data_list.append(cur.data)
            cur = cur.next
        return data_list

    def setUp(self):
        self.test_instance = TestClass()


    def tearDown(self):
        pass


    def testList(self):
        linkedlist = self.createLinkedlist([1, 2, 3, 4, 5])
        node = linkedlist.next.next.next
        self.test_instance.delNode(node)
        self.assertEqual([1, 2, 3, 5], self.printLinkedlist(linkedlist))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()