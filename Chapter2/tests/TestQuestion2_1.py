'''
Created on Jun 13, 2014

@author: Yin Song
'''
import unittest
from ..LinkedList import LinkedListNode as Node
from ..Question2_1 import Solution as TestClass

class Test(unittest.TestCase):

    def createLinkedlist(self, data_list):
        """
           create linked list.
        """
        head = Node(data_list[0])
        cur = head
        for data in data_list[1:]:
            cur.setNext(Node(data))
            cur = cur.next
        return head
    
    def printLinkedlist(self, head):
        """
           print linked list.
        """
        data_list = []
        cur = head
        while(cur != None):
            data_list.append(cur.data)
            cur = cur.next
        return data_list

    def testNoLinkedlist(self):
        """
          Test if the input is not LinkedList
        """
        linkedlist = [2,3,5,6]
        self.assertRaises(TypeError, TestClass.removeDup, linkedlist)
    
    def testNoDupLinkedlist(self):
        """
          Test if the input is not duplicate linked list
        """
        linkedlist = self.createLinkedlist(['1', '2', '3', '4', '5'])
        self.assertEqual(['1', '2', '3', '4', '5'], self.printLinkedlist(TestClass.removeDup(linkedlist)), "Not Duplicate linked list failed!!!")
        
    def testDupLinkedlist(self):
        """
          Test if the input is uplicate linked list
        """
        linkedlist = self.createLinkedlist(['1', '2', '4', '3', '4', '5'])
        #print self.printLinkedlist(TestClass.removeDup(linkedlist))
        self.assertEqual(['1', '2', '4', '3', '5'], self.printLinkedlist(TestClass.removeDup(linkedlist)), "Duplicate linked list failed!!!")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()