'''
Created on Jan 18, 2015

@author: Yin Song
'''
import unittest
from ..LinkedList import LinkedListNode as Node
from ..Question2_2 import Solution as TestClass

class Test(unittest.TestCase):
    def createLinkedlist(self, data_list):
        """
           create linked list.
        """
        head = Node(data_list[0])
        cur = head
        for data in data_list[1:]:
            cur.setNext(Node(data))
            cur = cur.next
        return head

    def setUp(self):
        self.test_instance = TestClass()


    def tearDown(self):
        pass


    def testType(self):
        linkedlist = ['1', '2', '3', '4', '5']
        N = 5
        self.assertRaises(TypeError, self.test_instance.findNth, linkedlist, N)
        
    def testShort(self):
        linkedlist = self.createLinkedlist([1, 2, 3, 4, 5])
        N = 6
        self.assertEqual(None, self.test_instance.findNth(linkedlist, N))
    
    def testNormal(self):
        linkedlist = self.createLinkedlist([1, 2, 3, 4, 5])
        N = 5
        self.assertEqual(1, self.test_instance.findNth(linkedlist, N))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()