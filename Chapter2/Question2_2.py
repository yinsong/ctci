'''
Created on Jan 18, 2015

@author: Yin Song
'''
from .LinkedList import LinkedListNode

class Solution(object):
    '''
    classdocs
    '''


    def findNth(self, linkedlist, N):
        '''
        Constructor
        '''
        # Check input
        if not isinstance(linkedlist, LinkedListNode):
            raise TypeError()
        
        # pointer 1
        p1 = linkedlist
        # pointer 2
        cnt = N-1
        p2 = linkedlist
        while (p2.next != None and cnt > 0):
            p2 = p2.next
            cnt -= 1
        
        # Check if list size < n
        if (cnt > 0):
            return None
        else:
            while (p2.next != None):
                p1 = p1.next
                p2 = p2.next
            return p1.data
                