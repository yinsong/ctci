'''
Created on Jun 13, 2014

@author: Yin Song
'''

class LinkedListNode:
    '''
    Linked List Node: next link + data
    '''

    def __init__(self, data):
        '''
        Constructor
        
        @param data: node data object 
        '''
        self.data = data
        self.next = None
    
    def setNext(self, next_node):
        """
          Set the next node of current node
        """
        # Check input
        if not isinstance(next_node, LinkedListNode):
            raise TypeError()
        self.next = next_node
        