'''
Created on Jan 18, 2015

@author: Yin Song
'''
import unittest
from ..Question1_8 import Solution as TestClass

class Test(unittest.TestCase):


    def setUp(self):
        self.test_instance = TestClass()


    def tearDown(self):
        pass


    def testSameString(self):
        s1 = 'waterbottle'
        s2 = 'waterbottle'
        
        self.assertTrue(self.test_instance.isRotation(s1, s2))
    
    def testRotateString(self):
        s1 = 'waterbottle'
        s2 = 'erbottlewat'
        
        self.assertTrue(self.test_instance.isRotation(s1, s2))
    
    def testNoRotateString(self):
        s1 = 'waterbottle'
        s2 = 'erbottlewateee'
        
        self.assertFalse(self.test_instance.isRotation(s1, s2))    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()