'''
Created on Jan 17, 2015

@author: Yin Song
'''
import unittest
from ..Question1_7 import Solution as TestClass

class Test(unittest.TestCase):


    def setUp(self):
        self.test_instance = TestClass()


    def tearDown(self):
        pass


    def testNoZeros(self):
        matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        self.test_instance.setZeroes(matrix)
        self.assertEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9]], matrix)

    def testHaveZeros(self):
        matrix = [[1, 0, 0], [4, 5, 6], [7, 8, 9]]
        self.test_instance.setZeroes(matrix)
        self.assertEqual([[0, 0, 0], [4, 0, 0], [7, 0, 0]], matrix)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()