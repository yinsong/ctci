'''
Created on Jan 17, 2015

@author: Yin Song
'''
import unittest
from ..Question1_6 import Solution as TestClass

class Test(unittest.TestCase):


    def setUp(self):
        self.test_instance = TestClass()


    def tearDown(self):
        pass


    def testThreeDimension(self):
        matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        matrix_rotated = [[7, 4, 1], [8, 5, 2], [9, 6, 3]]
        self.assertEqual(matrix_rotated, self.test_instance.rotate(matrix))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()