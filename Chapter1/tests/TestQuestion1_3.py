'''
Created on Jun 12, 2014

@author: Yin Song
'''
import unittest
from ..Question1_3 import Question1_3 as TestClass


class Test(unittest.TestCase):


    def testNoString(self):
        """Test if the input is not String"""
        string = 123
        self.assertRaises(TypeError, TestClass.removeDup, string)
        
    def testNullString(self):
        """Test if the input is Null String"""
        string = ''
        self.assertEqual(string, TestClass.removeDup(string), "Null String Test Failed!!")
        
    def testOneString(self):
        """Test if the input is one-char String"""
        string = 'a'
        self.assertEqual(string, TestClass.removeDup(string), "One Char String Test Failed!!")
        
    def testNoDupString(self):
        """Test if the input is No duplicate String"""
        string = 'abcd'
        self.assertEqual(string, TestClass.removeDup(string), "No duplicate String Test Failed!!")
    
    def testOneDupString(self):
        """Test if the input is One duplicate String"""
        string = 'aaaaa'
        self.assertEqual('a', TestClass.removeDup(string), "One duplicate String Test Failed!!")
        
    def testContiguousDupString(self):
        """Test if the input is contiguous duplicate String"""
        string = 'aaaabbbb'
        self.assertEqual('ab', TestClass.removeDup(string), "Contiguous duplicate String Test Failed!!")
        
    def testNonContiguousDupString(self):
        """Test if the input is Non contiguous duplicate String"""
        string = 'abcbac'
        self.assertEqual('abc', TestClass.removeDup(string), "Non contiguous duplicate String Test Failed!!")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testNullString']
    unittest.main()