'''
Created on Jun 12, 2014

@author: Yin Song
'''
import unittest
from ..Question1_4 import Question1_4 as TestClass


class Test(unittest.TestCase):


    def testNonString(self):
        """
          Test if the strings are not str
        """
        string1 = 123
        string2 = "abcd"
        self.assertRaises(TypeError, TestClass.isAnagram, string1, string2)
        self.assertRaises(TypeError, TestClass.isAnagramEff, string1, string2)
    
    def testNotString(self):
        """
          Test if the strings are not Anagram
        """
        string1 = "ddd"
        string2 = "abcd"
        self.assertFalse(TestClass.isAnagram(string1, string2), "Non Anagram Test Failed!!!!")
        self.assertFalse(TestClass.isAnagramEff(string1, string2), "Non Anagram Test Failed!!!!")
    
    def testAnagramString(self):
        """
          Test if the strings are Anagram
        """
        string1 = "dbac"
        string2 = "abcd"
        self.assertTrue(TestClass.isAnagram(string1, string2), "Anagram Test Failed!!!!")
        self.assertTrue(TestClass.isAnagramEff(string1, string2), "Anagram Test Failed!!!!")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testNonString']
    unittest.main()