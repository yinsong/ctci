'''
Created on Jun 12, 2014

@author: Yin Song
'''
import unittest
from ..Question1_2 import Question1_2 as TestClass


class Test(unittest.TestCase):


    def testName(self):
        string = "abcd "
        reversed_string = "dcba "
        self.assertEqual(reversed_string, TestClass.reverseString(string), "These are not the same")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()