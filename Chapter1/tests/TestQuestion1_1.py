'''
Created on Jun 12, 2014

@author: Yin Song
'''
import unittest
from ..Question1_1 import Question1_1 as TestClass


class Test(unittest.TestCase):
    
    
    def testUniqueString(self):
        """ Setting up for the test of Unique String"""
        test_instance = TestClass()
        string = "abcdefg"
        self.assertTrue(test_instance.isUnique(string), "The Unique String Test Fails!!!")
    
    def testNotUniqueString(self):
        """ Setting up for the test of Not Unique String"""
        test_instance = TestClass()
        string = "aaaaa"
        self.assertFalse(test_instance.isUnique(string), "The Not Unique String Test Fails!!!")
        
    def testNonString(self):
        """ Setting up for the test of Non Unique String"""
        test_instance = TestClass()
        string = 123
        self.assertRaises(TypeError, test_instance.isUnique, string)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testUniqueString']
    unittest.main()