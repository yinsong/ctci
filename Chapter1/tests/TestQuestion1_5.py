'''
Created on Jan 16, 2015

@author: Yin Song
'''
import unittest
from ..Question1_5 import replaceSpaces

class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass

    def testNullstring(self):
        self.assertEqual('', replaceSpaces(''))

    def testLeftspacestring(self):
        self.assertEqual('%20%20%20%20ddddd', replaceSpaces('    ddddd'))
    
    def testRightspacestring(self):
        self.assertEqual('ddddd%20%20%20%20', replaceSpaces('ddddd    '))
    
    def testMiddlespacestring(self):
        self.assertEqual('ddddd%20%20%20%20ddddd', replaceSpaces('ddddd    ddddd'))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testNullstring']
    unittest.main()