'''
Created on Jun 12, 2014

@author: Yin Song
'''

class Question1_4:
    '''
    Write a method to decide if two strings are anagrams or not.
    
    '''
    @classmethod
    def isAnagram(self, string1, string2):
        """
          This is a function to decide anagrams
        """
        # Check if string1, string2 are str type
        if (not isinstance(string1, str)) or (not isinstance(string2, str)):
            raise TypeError()
        # if not equal length
        if len(string1) != len(string2):
            return False
        # if they are same
        if string1 == string2:
            return True
        
        # transform string1, string2 to charlist
        char_list1 = list(string1)
        char_list2 = list(string2)
        
        # sort them
        char_list1.sort()
        char_list2.sort()
        
        if char_list1 == char_list2:
            return True
        else:
            return False
        
    @classmethod    
    def isAnagramEff(self, string1, string2):
        """
          This is an efficient version 
        """
        # Check if string1, string2 are str type
        if not(isinstance(string1, str) and isinstance(string2, str)):
            raise TypeError()
        # if not equal length
        if len(string1) != len(string2):
            return False
        # if they are same
        if string1 == string2:
            return True
        
        # transform string1, string2 to charlist
        char_list1 = list(string1)
        char_list2 = list(string2)
        
        # create a dict for store frequency of chars
        char_dict = dict()
        
        # for every element
        for i in range(len(string1)):
            if not char_dict.has_key(char_list1[i]):
                char_dict[char_list1[i]] = 1
            else:
                char_dict[char_list1[i]] += 1
            if not char_dict.has_key(char_list2[i]):
                char_dict[char_list2[i]] = -1
            else:
                char_dict[char_list2[i]] -= 1
        
        for key in char_dict.keys():
            if char_dict[key] != 0:
                return False
        
        return True
        
        
        