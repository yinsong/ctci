'''
Created on Jun 12, 2014

@author: Yin Song
'''

class Question1_3:
    '''
     Design an algorithm and write code to remove the duplicate characters in a string 
     without using any additional buffer. NOTE: One or two additional variables are fine.
     An extra copy of the array is not. FOLLOW UP Write the test cases for this method.
    '''

    @classmethod
    def removeDup(self, string):
        """
           remove the duplicate characters in a string.
           
        """
        # Check if it is not string
        if not isinstance(string, str):
            raise TypeError()
        # Check if it is null string or one-char string
        if len(string)<=1:
            return string
        
        # transform string to list
        char_list = list(string)
        # cursor that records end of new string
        cursor = 1
        # get the length of the char_list
        length = len(char_list)
        # for each element
        for i in range(1,length):
            # flag for duplicate
            flag = 0
            for j in range(0,cursor):
                if char_list[i] == char_list[j]:
                    flag = 1
            # if there is no duplicate, move the cursor 1 position right
            if flag == 0:
                char_list[cursor] = char_list[i]
                cursor += 1
        char_list_removed = char_list[0:cursor]
        string_removed = ''.join(char_list_removed)
        #print string_removed
        return string_removed
                
                