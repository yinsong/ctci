'''
Created on Jan 17, 2015

@author: Yin Song
'''
class Solution:
    # @param matrix, a list of lists of integers
    # @return a list of lists of integers
    def rotate(self, matrix):
        # Get the dimension of the matrix
        n = len(matrix)

        # For every layer of the matrix
        for i in range(0, n/2):
            # top to right
            cache = []
            for row in range(i, n-i-1):
                cache.append(matrix[row][n-1-i]) # store right
                matrix[row][n-1-i] = matrix[i][row]
            # left to top
            for row in range(i+1, n-i):
                matrix[i][n-1-row] = matrix[row][i]

            # bottom to left
            for col in range(i+1, n-i):
                matrix[col][i] = matrix[n-i-1][col]

            # right to bottom
            matrix[n-i-1][i+1:n-i] = reversed(cache)
        return matrix