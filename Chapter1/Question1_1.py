'''
Created on Jun 12, 2014

@author: Yin Song
'''

class Question1_1:
    '''
    Implement an algorithm to determine if a string has all unique
    characters. What if you can not use additional data structures?
    '''
    def isUnique(self, string):
        # check if it is not str.
        if (not isinstance(string, str)):
            raise TypeError()
            
        # string to char list.
        char_list = list(string)
        # create an empty dictionary to store chars
        char_dict = dict()
        for char in char_list:
            if (not char_dict.has_key(char)):
                char_dict[char] = 1
            else:
                return False
        
        return True
        