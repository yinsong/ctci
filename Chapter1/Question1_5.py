'''
Created on Jan 16, 2015

@author: Yin Song
'''

def replaceSpaces(string):
    string2 = ''
    for s in string:
        if (s == ' '):
            string2 += '%20'
        else:
            string2 += s
    return string2
    