'''
Created on Jan 17, 2015

@author: Yin Song
'''
class Solution:
    # @param matrix, a list of lists of integers
    # RETURN NOTHING, MODIFY matrix IN PLACE.
    def setZeroes(self, matrix):
        rows = []
        cols = []
        for i in range(0, len(matrix)):
            for j in range(0, len(matrix[0])):
                if matrix[i][j] == 0:
                    if (i not in rows):
                        rows.append(i)
                    if (j not in cols):
                        cols.append(j)
        for row in rows:
            matrix[row] = [0] * len(matrix[0])
        for col in cols:
            for i in range(len(matrix)):
                matrix[i][col] = 0