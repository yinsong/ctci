'''
Created on Jun 12, 2014

@author: Yin Song
'''

class Question1_2:
    '''
    Write code to reverse a C-Style String. (C-string means that "abcd" 
    is represented as five characters, including the null character.)
    '''

    @classmethod
    def reverseString(self, string):
        # transform string to char list
        char_list = list(string)
        # get the length of the char_list
        length = len(char_list)
        # from the first char and last second char, swap them
        for i in range(length/2):
            # swap the element
            temp = char_list[i]
            char_list[i] = char_list[length-2-i]
            char_list[length-2-i] = temp
        # transform char list to string
        reversed_string = ''.join(char_list)
        return reversed_string
            
        
        