'''
Created on Jan 18, 2015

@author: Yin Song
'''

class Solution(object):
    '''
    classdocs
    '''


    def isSubstring(self, word1, word2):
        '''
        @param word1: str
               word2: str
        return True if word1 is a substring of word2, False else. 
        '''
        if word1 in word2:
            return True
        else:
            return False
    
    def isRotation(self, s1, s2):
        '''
        @param s1: str
               s2: str
        return True if s2 is a rotation of s1, False else. 
        '''
        # Check s1 and s2 are equal length and not empty
        if (len(s1) == len(s2) and len(s1) > 0):
            return self.isSubstring(s1, s2+s2)
        
        return False 